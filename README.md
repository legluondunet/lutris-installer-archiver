# Lutris Installer Archiver

This is a simple nodejs program to crawl the lutris API and backup all the game metadata and installers. You can see an example of what it generates here: https://gitlab.com/smichel17/lutris-installer-archive

## Quick start

```
mkdir archive
cd archive
git init
cd ..
git clone https://gitlab.com/smichel17/lutris-installer-archiver archiver
cd archiver
npm install
npm start -- ../archive
cd ../archive
git commit -m "$(date)"
```

## TODO

Currently this is an inefficient implementation -- it completely removes the archive directory, then recreates everything from scratch. Incremental updates could be added later (although the main bottleneck at this point is the API rate limit, so this isn't super important).

## License

This project is licensed under the GNU General Public License version 3, or (at your option) any later version.
